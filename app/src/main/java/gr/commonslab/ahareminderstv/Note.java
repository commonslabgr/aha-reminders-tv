package gr.commonslab.ahareminderstv;

public class Note {

    private int notesID;
    private String fileName;
    private String title;
    private String message;
    private String answers;
    private String createdDate;
    private String scheduledDateTime;
    //  private String scheduledTime;

    public Note(int notesID, String fileName, String title, String message, String answers, String createdDate, String scheduledDateTime) {
        this.notesID = notesID;
        this.fileName = fileName;
        this.title = title;
        this.message = message;
        this.answers = answers;
        this.createdDate = createdDate;
        this.scheduledDateTime = scheduledDateTime;
        //  this.scheduledTime = scheduledTime;
    }

    @Override
    public String toString() {
        return "Note{" +
                "notesID=" + notesID +
                ", fileName='" + fileName + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", answers='" + answers + '\'' +
                ", createdDate=" + createdDate +
                ", scheduledDate=" + scheduledDateTime +

                '}';
    }

    public int getNotesID() {
        return notesID;
    }

    public void setNotesID(int notesID) {
        this.notesID = notesID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getScheduledDateTime() {
        return scheduledDateTime;
    }


    public void setScheduledDateTime(String scheduledDateTime) {
        this.scheduledDateTime = scheduledDateTime;
    }


}
