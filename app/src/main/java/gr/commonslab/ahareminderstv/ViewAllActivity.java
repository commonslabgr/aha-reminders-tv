package gr.commonslab.ahareminderstv;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ViewAllActivity extends AppCompatActivity {
    DatabaseHelper databaseHelper;

    //this activity checks the type of file and calls the apropriate activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(ViewAllActivity.this);
        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);

        //finding file type
        Uri file = databaseHelper.getPath(databaseHelper.getId(date));
        //removing spaces from Uri because it cant get extension
        String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString().replace(" ", "%20"));

        if (fileExt.equals("mp4")) {
            Intent intent = new Intent(ViewAllActivity.this, VideoActivity.class);
            startActivity(intent);
        } else if (fileExt.equals("mp3")) {
            Intent intent = new Intent(ViewAllActivity.this, AudioActivity.class);
            startActivity(intent);

        } else if (fileExt.equals("jpg")) {
            Intent intent = new Intent(ViewAllActivity.this, ImageActivity.class);
            startActivity(intent);

        }else if (fileExt.equals("txt")){
            Intent intent = new Intent(ViewAllActivity.this,TextActivity.class);
            startActivity(intent);
        }

    }

}