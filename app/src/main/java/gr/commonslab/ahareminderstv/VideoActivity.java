package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VideoActivity extends AppCompatActivity {
    VideoView videoView;
    Button RspButton1, RspButton2, RspButton3;
    DatabaseHelper databaseHelper;
    String[] answers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        String dir;
        videoView = findViewById(R.id.videoView);
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        databaseHelper = new DatabaseHelper(VideoActivity.this);
        androidx.gridlayout.widget.GridLayout gridLayout = findViewById(R.id.gridLayout);

        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);

        String curVideo;

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            curVideo = String.valueOf((int) bundle.get("file"));

        } else {
            curVideo = databaseHelper.getId(date);
        }

        RspButton1 = findViewById(R.id.RspBtn1);
        RspButton2 = findViewById(R.id.RspBtn2);
        RspButton3 = findViewById(R.id.RspBtn3);
        answers = databaseHelper.displayAnswers(curVideo);//returns list with possible responses of note'

        if (answers.length == 1) {
            gridLayout.setVisibility(View.VISIBLE);
            RspButton2.setVisibility(Button.GONE);
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton1.getText().toString(), curVideo);
                }
            });

        } else if (answers.length == 2) {
            gridLayout.setVisibility(View.VISIBLE);
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curVideo);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curVideo);
                }
            });

        } else if (answers.length == 3) {
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton3.setText(answers[2]);


            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curVideo);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curVideo);
                }
            });
            RspButton3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton3.getText().toString(), curVideo);
                }
            });

        }
        videoPlayer();


    }

    private void videoPlayer() {
        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);
        String curVideo;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            curVideo = String.valueOf((int) bundle.get("file"));

        } else {
            curVideo = databaseHelper.getId(date);
        }

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        //raw folder
        //videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.videotest));
        //data folder
        videoView.setVideoPath(databaseHelper.getPath(curVideo).toString());

        videoView.start();

    }
}