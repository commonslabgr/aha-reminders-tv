package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageActivity extends AppCompatActivity {
    Button RspButton1, RspButton2, RspButton3;
    DatabaseHelper databaseHelper;
    String[] answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media2);
        databaseHelper = new DatabaseHelper(ImageActivity.this);
        ImageView imageView;
        imageView = findViewById(R.id.imageView);
        RspButton1 = findViewById(R.id.RspBtn1);
        RspButton2 = findViewById(R.id.RspBtn2);
        RspButton3 = findViewById(R.id.RspBtn3);


        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);

        String curImage;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            curImage = String.valueOf((int) bundle.get("file"));

        } else {
            curImage = databaseHelper.getId(date);
        }

        Bitmap myBitmap = BitmapFactory.decodeFile(databaseHelper.getPath(curImage).toString());
        imageView.setImageBitmap(myBitmap);

        answers = databaseHelper.displayAnswers(curImage);//returns list with possible responses of note
        if (answers.length == 1) {
            RspButton2.setVisibility(Button.GONE);
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton1.getText().toString(), curImage);
                }
            });

        } else if (answers.length == 2) {
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curImage);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curImage);
                }
            });

        } else if (answers.length == 3) {
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton3.setText(answers[2]);

            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curImage);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curImage);
                }
            });
            RspButton3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton3.getText().toString(), curImage);
                }
            });

        }

    }

}