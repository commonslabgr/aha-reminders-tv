package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddNewText extends AppCompatActivity {
    Button addNote;
    EditText titleInp, messageInp, scheduledDate, scheduledTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_text);

        titleInp = findViewById(R.id.titleEd);
        messageInp = findViewById(R.id.msgEd);
        scheduledDate = findViewById(R.id.scheduledEd);
        scheduledTime = findViewById(R.id.scheduledTimeEd);

        scheduledDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(AddNewText.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(selectedyear, selectedmonth, selectedday);
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        String dateString = format.format(calendar.getTime());
                        scheduledDate.setText(dateString);

                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();


            }
        });
        scheduledTime.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;

            @Override
            public void onClick(View view) {

                mTimePicker = new TimePickerDialog(AddNewText.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        scheduledTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);// 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        addNote = findViewById(R.id.AddNotebtn);


        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] answers = new String[1];

                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AddNewText.this);
                bottomSheetDialog.setContentView(R.layout.bottom_sheet);
                EditText response1 = bottomSheetDialog.findViewById(R.id.Resposne1);
                EditText response2 = bottomSheetDialog.findViewById(R.id.Response2);
                EditText response3 = bottomSheetDialog.findViewById(R.id.Response3);
                Button addResponse = bottomSheetDialog.findViewById(R.id.AddResponsesBtn);

                addResponse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        answers[0] = response1.getText().toString() + "-" + response2.getText().toString() + "-"
                                + response3.getText().toString();
                        DatabaseHelper db = new DatabaseHelper(AddNewText.this);
                        db.addNote(".txt", titleInp.getText().toString(), messageInp.getText().toString(), scheduledDate.getText().toString()
                                + " " + scheduledTime.getText().toString(), answers[0]);
                        finish();
                    }
                });
                bottomSheetDialog.show();
            }
        });

    }


}