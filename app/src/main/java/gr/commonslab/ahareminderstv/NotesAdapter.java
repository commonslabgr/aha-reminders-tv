package gr.commonslab.ahareminderstv;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    Context context;
    RecyclerView recyclerView;
    List<Note> noteList;

    final View.OnClickListener onClickListener = new MyOnClickListener();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView rowId;
        TextView rowTitle;
        TextView rowDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rowId = itemView.findViewById(R.id.noteId);
            rowTitle = itemView.findViewById(R.id.noteTitle);
            rowDate = itemView.findViewById(R.id.noteScheduleDate);

        }
    }

    public NotesAdapter(Context context, List<Note> noteList, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.noteList = noteList;
    }

    @NonNull
    @Override
    public NotesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row, parent, false);
        view.setOnClickListener(onClickListener);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotesAdapter.ViewHolder viewHolder, int position) {

        Note note = noteList.get(position);
        viewHolder.rowId.setText("" + note.getNotesID());
        viewHolder.rowTitle.setText(note.getTitle());
        viewHolder.rowDate.setText(note.getScheduledDateTime());
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }


    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            int itemPosition = recyclerView.getChildLayoutPosition(view);
            DatabaseHelper databaseHelper = new DatabaseHelper(context);

            int noteId = noteList.get(itemPosition).getNotesID();
            Uri file = databaseHelper.getPath(String.valueOf(noteId));
            String fileExt = MimeTypeMap.getFileExtensionFromUrl(file.toString().replace(" ", "%20"));

            if (fileExt.equals("mp4")) {
                Intent intent = new Intent(context, VideoActivity.class);
                intent.putExtra("file", noteId);
                context.startActivity(intent);
            } else if (fileExt.equals("mp3")) {
                Intent intent = new Intent(context, AudioActivity.class);
                intent.putExtra("file", noteId);
                context.startActivity(intent);

            } else if (fileExt.equals("jpg")) {
                Intent intent = new Intent(context, ImageActivity.class);
                intent.putExtra("file", noteId);
                context.startActivity(intent);

            }else if (fileExt.equals("txt")){
                Intent intent = new Intent(context,TextActivity.class);
                intent.putExtra("file", noteId);
                context.startActivity(intent);
            }
        }
    }
}
