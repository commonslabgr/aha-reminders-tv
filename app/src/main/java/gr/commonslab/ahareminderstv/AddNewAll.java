package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class AddNewAll extends AppCompatActivity {
    Button addTxt, addVideo, addAudio, addImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_all);

        addTxt = findViewById(R.id.NewTxtBtn);
        addVideo = findViewById(R.id.NewVidBtn);
        addAudio = findViewById(R.id.NewAudioBtn);
        addImage = findViewById(R.id.NewImageBtn);



        addTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddNewAll.this, AddNewText.class);
                startActivity(intent);
            }
        });
        addVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddNewAll.this, AddNewVideo.class);
                startActivity(intent);
            }
        });
        addAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddNewAll.this, AddNewAudio.class);
                startActivity(intent);
            }
        });
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddNewAll.this, AddNewImage.class);
                startActivity(intent);
            }
        });

    }
}