package gr.commonslab.ahareminderstv;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static android.os.Environment.DIRECTORY_MOVIES;

public class AddNewVideo extends AppCompatActivity {
    Button addVidNote, recordBtn, selectBtn;
    EditText addVidTitle, addVidDate, addVidTime;
    String currentVideoPath;
    private ActivityResultLauncher<Intent> activityResultLauncher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_video);
        addVidTitle = findViewById(R.id.VidTitleED);
        recordBtn = findViewById(R.id.CaptureVidBtn);
        selectBtn = findViewById(R.id.SelectVidBtn);
        addVidDate = findViewById(R.id.VidSchDate);
        addVidTime = findViewById(R.id.VidSchTime);
        addVidNote = findViewById(R.id.addNewVidNoteBtn);

        //starting activity to capture photo
        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {

                    }
                });
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri uri) {
                        currentVideoPath = String.valueOf(uri);
                    }
                });


        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);

                File videoFile = null;
                try {
                    videoFile = createVideoFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (videoFile != null) {
                    Uri videoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                            BuildConfig.APPLICATION_ID + ".provider", videoFile);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI);
                    activityResultLauncher.launch(intent);
                }
            }
        });
        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Video.Media.INTERNAL_CONTENT_URI);
                intent.setType("*/*");
                mGetContent.launch("video/*");
            }
        });

        addVidDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(AddNewVideo.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(selectedyear, selectedmonth, selectedday);
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        String dateString = format.format(calendar.getTime());
                        addVidDate.setText(dateString);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });
        addVidTime.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;

            @Override
            public void onClick(View view) {

                mTimePicker = new TimePickerDialog(AddNewVideo.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        addVidTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);// 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        addVidNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] answers = new String[1];

                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AddNewVideo.this);
                bottomSheetDialog.setContentView(R.layout.bottom_sheet);
                EditText response1 = bottomSheetDialog.findViewById(R.id.Resposne1);
                EditText response2 = bottomSheetDialog.findViewById(R.id.Response2);
                EditText response3 = bottomSheetDialog.findViewById(R.id.Response3);
                Button addResponse = bottomSheetDialog.findViewById(R.id.AddResponsesBtn);

                addResponse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        answers[0] = response1.getText().toString() + "-" + response2.getText().toString() + "-"
                                + response3.getText().toString();
                        DatabaseHelper db = new DatabaseHelper(AddNewVideo.this);
                        db.addNote(currentVideoPath, addVidTitle.getText().toString(), null, addVidDate.getText().toString()
                                + " " + addVidTime.getText().toString(), answers[0]);
                        finish();
                    }
                });
                bottomSheetDialog.show();
            }
        });

    }

    private File createVideoFile() throws IOException {
        // Create an video file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String videoFileName = "MP4" + timeStamp + "_";
        File storageDir = getExternalFilesDir(DIRECTORY_MOVIES);
        File video = File.createTempFile(
                videoFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentVideoPath = video.getPath();
        return video;
    }

}