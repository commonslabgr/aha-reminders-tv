package gr.commonslab.ahareminderstv;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class DatabaseHelper extends SQLiteOpenHelper {
    List<Note> noteList = new ArrayList<>();
    private static final String COLUMN_NOTESFK_ID = "NOTESFK_ID";
    private static final String COLUMN_SCHEDULED_TIMEFK = "SCHEDULED_TIMEFK";
    private static final String COLUMN_SCHEDULED_ID = "SCHEDULED_ID";
    private Context context;
    public static final String NOTES_TABLE = "NOTES_TABLE";
    public static final String COLUMN_FILENAME = "FILENAME";
    public static final String COLUMN_TITLE = "TITLE";
    public static final String COLUMN_MESSAGE = "MESSAGE";
    public static final String COLUMN_ANSWERS = "ANSWERS";
    public static final String COLUMN_NOTES_ID = "ID";
    private static final String SCHEDULE_TABLE = "SCHEDULE_TABLE";
    private static final String COLUMN_SCHEDULED_TIME = "SCHEDULED_TIME";
    private static final String COLUMN_REPEAT = "REPEAT";
    private static final String RESPONSE_TABLE = "RESPONSE_TABLE";
    private static final String COLUMN_CREATED_TIME = "CREATED_TIME";
    private static final String COLUMN_RESPONSE = "RESPONSE";


    public DatabaseHelper(@Nullable Context context) {
        super(context, "notes.db", null, 1);
        this.context = context;
    }

    //this is the first time a database is accessed.there should be code to create new database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query1 = "CREATE TABLE " + NOTES_TABLE +
                "(" + COLUMN_NOTES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_FILENAME + " TEXT," +
                COLUMN_TITLE + " TEXT," +
                COLUMN_MESSAGE + " TEXT," +
                COLUMN_ANSWERS + " TEXT)";
        String query2 = "CREATE TABLE " + SCHEDULE_TABLE +
                "(" + COLUMN_SCHEDULED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_SCHEDULED_TIME + " TEXT," +
                COLUMN_REPEAT + " TEXT," +
                COLUMN_NOTESFK_ID + " INT,"
                + " FOREIGN KEY (" + COLUMN_NOTESFK_ID + ") REFERENCES " + NOTES_TABLE + "(" + COLUMN_NOTES_ID + "));";
        String query3 = "CREATE TABLE " + RESPONSE_TABLE +
                "(" + COLUMN_CREATED_TIME + " TEXT PRIMARY KEY," +
                COLUMN_RESPONSE + " TEXT," +
                COLUMN_NOTESFK_ID + " INT," +
                COLUMN_SCHEDULED_TIMEFK + " INT," +
                "FOREIGN KEY(" + COLUMN_NOTESFK_ID + ") REFERENCES " + NOTES_TABLE + "(" + COLUMN_NOTES_ID + "), " +
                "FOREIGN KEY(" + COLUMN_SCHEDULED_TIMEFK + ") REFERENCES " + SCHEDULE_TABLE + "(" + COLUMN_SCHEDULED_TIME + "));";

        db.execSQL(query1);
        db.execSQL(query2);
        db.execSQL(query3);

    }

    //this is called if the database version number changes. it prevents previous users apps from braking when you change database design
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean addNote(String filePath, String title, String message, String scheduledTime, String answers) {

        SQLiteDatabase db = this.getWritableDatabase();
        db = this.getReadableDatabase();

        ContentValues cv = new ContentValues();
        ContentValues cv2 = new ContentValues();
        ContentValues cv3 = new ContentValues();

        cv.put(COLUMN_FILENAME, filePath);
        cv.put(COLUMN_TITLE, title);
        cv.put(COLUMN_MESSAGE, message);
        cv.put(COLUMN_ANSWERS, answers);
        long insert = db.insert(NOTES_TABLE, null, cv);

        cv2.put(COLUMN_SCHEDULED_TIME, scheduledTime);
        cv2.put(COLUMN_NOTESFK_ID, insert);
        long insert2 = db.insert(SCHEDULE_TABLE, null, cv2);

        cv3.put(COLUMN_NOTESFK_ID, insert);
        cv3.put(COLUMN_SCHEDULED_TIMEFK, insert2);

        long insert3 = db.insert(RESPONSE_TABLE, null, cv3);

        if (insert == -1 & insert2 == -1 & insert3 == -1) {
            Toast.makeText(context, "failed ", Toast.LENGTH_SHORT).show();
            return false;

        } else {
            Toast.makeText(context, "Success ", Toast.LENGTH_SHORT).show();
            return true;
        }

    }

    //display notes available answers
    public String[] displayAnswers(String noteId) {
        Cursor cursor = null;
        ArrayList<String> answers = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        if (db != null) {
            cursor = db.rawQuery("select " + COLUMN_ANSWERS + " from NOTES_TABLE where " + COLUMN_NOTES_ID + "=?", new String[]{noteId});
            if (cursor.moveToFirst()) { //<<<< Move to the first row (should only be 1)
                String[] beforeSplit = cursor.getString(cursor.getColumnIndex(COLUMN_ANSWERS)).split("-");//<< get  data from the column
                for (int i = 0; i < beforeSplit.length; i++) {
                    answers.add(beforeSplit[i]);
                }
            }
        }
        cursor.close();
        return answers.toArray(new String[answers.size()]);
    }

    //function to check if current date matches existing note created date
    public boolean isNote(String curDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        String queryString = "SELECT 1 FROM " + SCHEDULE_TABLE + " WHERE " + COLUMN_SCHEDULED_TIME + " = ?";
        Cursor c = db.rawQuery(queryString, new String[]{curDate});
        boolean result = c.getCount() > 0;
        c.close();

        return result;
    }

    //function to get id of note of current date
    public String getId(String curDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        String noteId;
        String queryString = "SELECT " + COLUMN_NOTESFK_ID + " FROM " + SCHEDULE_TABLE + " WHERE " + COLUMN_SCHEDULED_TIME + " = ?";
        Cursor c = db.rawQuery(queryString, new String[]{curDate});
        if (c.moveToFirst()) {
            c.moveToFirst();
            noteId = c.getString(0);
        } else noteId = null;
        return noteId;
    }

    //function to return list of notes titles of current date
    public List<Note> getUpcomingNotes() {
        SQLiteDatabase db = this.getReadableDatabase();
        String columns[] = {DatabaseHelper.COLUMN_NOTES_ID, DatabaseHelper.COLUMN_TITLE};
        String column2[] = {DatabaseHelper.COLUMN_SCHEDULED_TIME};

        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Cursor c = db.query(DatabaseHelper.NOTES_TABLE, columns, null, null, null, null, null);
        Cursor c2 = db.query(DatabaseHelper.SCHEDULE_TABLE, column2, null, null, null, null, null);

        while (c.moveToNext() && c2.moveToNext()) {
            int index1 = c.getColumnIndex(DatabaseHelper.COLUMN_NOTES_ID);
            int rowid = c.getInt(index1);
            int index2 = c.getColumnIndex(DatabaseHelper.COLUMN_TITLE);
            String title = c.getString(index2);
            int index3 = c2.getColumnIndex(DatabaseHelper.COLUMN_SCHEDULED_TIME);
            String scheduledDateTime = c2.getString(index3);
            String temp = scheduledDateTime.substring(0, 10);
            for (int i = 0; i <= 5; i++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(todaysdate);
                calendar.add(Calendar.DATE, i);
                String datePlusOne = format.format(calendar.getTime());
                if (temp.equals(datePlusOne)) {
                    Note note = new Note(rowid, null, title, null, null, null, scheduledDateTime);
                    noteList.add(note);
                }
            }
        }
        return noteList;
    }

    //function to add response message to response table
    public void addResponse(String response, String noteId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String date = format.format(todaysdate);

        cv.put(COLUMN_CREATED_TIME, date);
        cv.put(COLUMN_RESPONSE, response);
        db.update(RESPONSE_TABLE, cv, COLUMN_NOTESFK_ID + " = ?", new String[]{noteId});

    }


    //function to get file path from column filename
    Uri getPath(String noteId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Uri filepath;
        Cursor cursor = null;
        cursor = db.rawQuery("select " + COLUMN_FILENAME + " from NOTES_TABLE where " + COLUMN_NOTES_ID + "=?", new String[]{noteId});
        cursor.moveToFirst();
        filepath = Uri.parse(cursor.getString(0));
        return filepath;
    }

    public String getNoteTitle(String noteId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String noteTitle;
        String queryString = "SELECT " + COLUMN_TITLE + " FROM " + NOTES_TABLE + " WHERE " + COLUMN_NOTES_ID + " = ?";
        Cursor c = db.rawQuery(queryString, new String[]{noteId});
        if (c.moveToFirst()) {
            c.moveToFirst();
            noteTitle = c.getString(0);
        } else noteTitle = null;
        return noteTitle;
    }
    public String getNoteMessage(String noteId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String noteMsg;
        String queryString = "SELECT " + COLUMN_MESSAGE + " FROM " + NOTES_TABLE + " WHERE " + COLUMN_NOTES_ID + " = ?";
        Cursor c = db.rawQuery(queryString, new String[]{noteId});
        if (c.moveToFirst()) {
            c.moveToFirst();
            noteMsg = c.getString(0);
        } else noteMsg = null;
        return noteMsg;
    }


}
