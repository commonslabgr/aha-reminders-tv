package gr.commonslab.ahareminderstv;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    //reference to buttons and other controls

    FloatingActionButton AddNew;
    DatabaseHelper databaseHelper;
    RecyclerView recyclerView;
    NotesAdapter notesAdapter;
    RecyclerView.LayoutManager layoutManager;
    List<Note> noteList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        File s = getFilesDir();
        databaseHelper = new DatabaseHelper(MainActivity.this);


        noteList = databaseHelper.getUpcomingNotes();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        notesAdapter = new NotesAdapter(this, noteList, recyclerView);
        recyclerView.setAdapter(notesAdapter);

        AddNew = findViewById(R.id.floatingActionButton3);


        AddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddNewAll.class);
                startActivity(intent);
            }
        });

        startThread();


        //                        shell commands


        //   Process p;
        // try {
        //       p = Runtime.getRuntime().exec(new String[] { "chmod 775 /sys/class/cec/" });
        //ProcessBuilder builder = new ProcessBuilder("su", "-c", "echo 0x40 0x44 0x41 > /sys/class/cec/cmd");
        // builder.command("echo 0x40 0x44 0x41 > /sys/class/cec/cmd");
        // builder.redirectErrorStream(true);

        //p = builder.start();
        // DataOutputStream os = new DataOutputStream(p.getOutputStream());


        //   os.writeBytes("su" + "\n");
        //  os.writeBytes("echo 0x40 0x36> /sys/class/cec/cmd" + "\n");
        //os.writeBytes(> /sys/class/cec/cmd" + "\n");

        //os.writeBytes("exit\n");
        //os.flush();
        //} catch (IOException e) {
        //  e.printStackTrace();
        //}


        //          volume commands
        // volume up  echo 0x40 0x44 0x41
        // volume down echo 0x40 0x44 0x42
        // volume stop echo 0x40 0x44 0x40

        //           stand by
        // echo 0x40 0x36


        //          change to tv turner
        //echo 0x40 9D 00 00

        //          change to hdmi source
        //echo 0x4F 0x82 10 00


        //          device power status
        // 0x40 08F (not working?)

        //          read from registry
        //cat /sys/class/cec/dump_reg


    }


    @Override
    public void onResume() {
        super.onResume();
        noteList.clear();
        noteList = databaseHelper.getUpcomingNotes();
        notesAdapter.notifyDataSetChanged();

    }

    //thread to check every 30secs for note scheduled time
    public void startThread() {
        thread t1 = new thread();
        t1.start();
    }

    class thread extends Thread {


        //checking when opening app for scheduled notes after given seconds
        private final Handler mHandler = new Handler();

        @Override
        public void run() {
            Date todaysdate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
            String date = format.format(todaysdate);
            if (databaseHelper.isNote(date)) {
                Intent intent = new Intent(MainActivity.this, ViewAllActivity.class);
                startActivity(intent);
            }
            mHandler.postDelayed(this, 30000);
        }
    }

}


