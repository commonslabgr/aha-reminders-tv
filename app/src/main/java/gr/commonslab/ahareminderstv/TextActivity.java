package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TextActivity extends AppCompatActivity {
    Button RspButton1, RspButton2, RspButton3;
    DatabaseHelper databaseHelper;
    TextView textTitle,textMsg;
    String[] answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);
        databaseHelper = new DatabaseHelper(TextActivity.this);
        RspButton1 = findViewById(R.id.RspBtnText1);
        RspButton2 = findViewById(R.id.RspBtnText2);
        RspButton3 = findViewById(R.id.RspBtnText3);
        textTitle = findViewById(R.id.TextTitle);
        textMsg =findViewById(R.id.TextMessage);

        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);


        String curText;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            curText = String.valueOf((int) bundle.get("file"));

        } else {
            curText = databaseHelper.getId(date);
        }
        answers = databaseHelper.displayAnswers(curText);

        textTitle.setText(databaseHelper.getNoteTitle(curText));
        textMsg.setText(databaseHelper.getNoteMessage(curText));

        if (answers.length == 1) {
            RspButton2.setVisibility(Button.GONE);
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton1.getText().toString(), curText);
                }
            });

        } else if (answers.length == 2) {
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curText);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curText);
                }
            });

        } else if (answers.length == 3) {
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton3.setText(answers[2]);

            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curText);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curText);
                }
            });
            RspButton3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton3.getText().toString(), curText);
                }
            });

        }


    }
}