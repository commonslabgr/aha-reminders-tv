package gr.commonslab.ahareminderstv;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static android.os.Environment.DIRECTORY_PICTURES;

public class AddNewImage extends AppCompatActivity {
    Button takePhoto, selectImage, addNote;
    String currentPhotoPath;
    EditText imgTitle, imgMsg, imgSchTime, imgSchDate;

    private ActivityResultLauncher<Intent> activityResultLauncher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_image);
        takePhoto = findViewById(R.id.CameraBtn);
        selectImage = findViewById(R.id.SelectImageBtn);
        addNote = findViewById(R.id.AddImageNotebtn);
        imgTitle = findViewById(R.id.NoteTitleImgET);
        imgMsg = findViewById(R.id.NoteMsgET);
        imgSchDate = findViewById(R.id.ImgScheduledEd);
        imgSchTime = findViewById(R.id.ImgScheduledTimeEd);
        addNote = findViewById(R.id.AddImageNotebtn);

        //starting activity to capture photo
        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {

                    }
                });
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri uri) {
                        currentPhotoPath = String.valueOf(uri);
                    }
                });

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                            BuildConfig.APPLICATION_ID + ".provider", photoFile);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    activityResultLauncher.launch(intent);

                }
            }
        });

        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("*/*");
                mGetContent.launch("image/*");


            }
        });

        imgSchDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(AddNewImage.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(selectedyear, selectedmonth, selectedday);
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        String dateString = format.format(calendar.getTime());
                        imgSchDate.setText(dateString);

                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();

            }
        });
        imgSchTime.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;

            @Override
            public void onClick(View view) {

                mTimePicker = new TimePickerDialog(AddNewImage.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        imgSchTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);// 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] answers = new String[1];

                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AddNewImage.this);
                bottomSheetDialog.setContentView(R.layout.bottom_sheet);
                EditText response1 = bottomSheetDialog.findViewById(R.id.Resposne1);
                EditText response2 = bottomSheetDialog.findViewById(R.id.Response2);
                EditText response3 = bottomSheetDialog.findViewById(R.id.Response3);
                Button addResponse = bottomSheetDialog.findViewById(R.id.AddResponsesBtn);

                addResponse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        answers[0] = response1.getText().toString() + "-" + response2.getText().toString() + "-"
                                + response3.getText().toString();
                        DatabaseHelper db = new DatabaseHelper(AddNewImage.this);
                        db.addNote(currentPhotoPath, imgTitle.getText().toString(), imgMsg.getText().toString(), imgSchDate.getText().toString()
                                + " " + imgSchTime.getText().toString(), answers[0]);
                        finish();
                    }
                });
                bottomSheetDialog.show();
            }
        });

    }

    //method to save image to internal storage

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getPath();
        return image;
    }

}