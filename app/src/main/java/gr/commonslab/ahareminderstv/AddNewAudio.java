package gr.commonslab.ahareminderstv;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Environment.DIRECTORY_MUSIC;


public class AddNewAudio extends AppCompatActivity {
    Button recordBtn, stopRecordBtn, addAudioNote, selectAudio;
    EditText addAudioDate, addAudioTime, addAudioTitle, addAudioMsg;
    MediaRecorder mRecorder;
    String currentAudioPath;

    private static String mFileName = null;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_audio);
        recordBtn = findViewById(R.id.recordAudio);
        stopRecordBtn = findViewById(R.id.stopRecord);
        addAudioTitle = findViewById(R.id.audioTItle);
        addAudioMsg = findViewById(R.id.audioMsg);
        addAudioNote = findViewById(R.id.addNote);
        addAudioDate = findViewById(R.id.scheduledAudioDate);
        addAudioTime = findViewById(R.id.scheduledAudioTime);
        selectAudio = findViewById(R.id.selectAudio);

        stopRecordBtn.setEnabled(false);
        recordBtn.setEnabled(true);

        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri uri) {
                        currentAudioPath = String.valueOf(uri);
                    }
                });

        try {
            mFileName = String.valueOf(createAudioFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(mFileName);


        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckPermissions()) {

                    try {
                        mRecorder.prepare();

                    } catch (IOException e) {

                    }
                    mRecorder.start();
                    stopRecordBtn.setEnabled(true);
                    recordBtn.setEnabled(false);

                    Toast.makeText(getApplicationContext(), "Recording Started", Toast.LENGTH_LONG).show();
                } else {
                    RequestPermissions();
                }
            }
        });
        stopRecordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecordBtn.setEnabled(false);
                recordBtn.setEnabled(true);

                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
                Toast.makeText(getApplicationContext(), "Recording Stopped", Toast.LENGTH_LONG).show();
            }
        });
        selectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
                intent.setType("*/*");
                mGetContent.launch("audio/*");

            }
        });
        addAudioDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(AddNewAudio.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(selectedyear, selectedmonth, selectedday);
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        String dateString = format.format(calendar.getTime());
                        addAudioDate.setText(dateString);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });
        addAudioTime.setOnClickListener(new View.OnClickListener() {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;

            @Override
            public void onClick(View view) {

                mTimePicker = new TimePickerDialog(AddNewAudio.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        addAudioTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                }, hour, minute, true);// 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        addAudioNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] answers = new String[1];

                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(AddNewAudio.this);
                bottomSheetDialog.setContentView(R.layout.bottom_sheet);
                EditText response1 = bottomSheetDialog.findViewById(R.id.Resposne1);
                EditText response2 = bottomSheetDialog.findViewById(R.id.Response2);
                EditText response3 = bottomSheetDialog.findViewById(R.id.Response3);
                Button addResponse = bottomSheetDialog.findViewById(R.id.AddResponsesBtn);


                addResponse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        answers[0] = response1.getText().toString() + "-" + response2.getText().toString() + "-"
                                + response3.getText().toString();
                        DatabaseHelper db = new DatabaseHelper(AddNewAudio.this);
                        db.addNote(currentAudioPath, addAudioTitle.getText().toString(), addAudioMsg.getText().toString(), addAudioDate.getText().toString()
                                + " " + addAudioTime.getText().toString(), answers[0]);
                        finish();
                    }
                });
                bottomSheetDialog.show();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private File createAudioFile() throws IOException {
        // Create an audio file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String audioFileName = "MP3_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(DIRECTORY_MUSIC);
        File audio = File.createTempFile(
                audioFileName,  /* prefix */
                ".mp3",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentAudioPath = audio.getPath();
        return audio;
    }

    public boolean CheckPermissions() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestPermissions() {
        ActivityCompat.requestPermissions(AddNewAudio.this, new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE},
                REQUEST_AUDIO_PERMISSION_CODE);
    }

}