package gr.commonslab.ahareminderstv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class AudioActivity extends AppCompatActivity {

    private ImageButton forwardbtn, backwardbtn, pausebtn, playbtn;
    private Button RspButton1, RspButton2, RspButton3;
    private MediaPlayer mPlayer;
    private TextView songName, startTime, songTime;
    private SeekBar songPrgs;
    private static int oTime = 0, sTime = 0, eTime = 0, fTime = 5000, bTime = 5000;
    private Handler hdlr = new Handler();
    DatabaseHelper databaseHelper;
    String[] answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        databaseHelper = new DatabaseHelper(AudioActivity.this);
        forwardbtn = findViewById(R.id.btnForward);
        backwardbtn = findViewById(R.id.btnBackward);
        pausebtn = findViewById(R.id.btnPause);
        playbtn = findViewById(R.id.btnPlay);
        RspButton1 = findViewById(R.id.RspBtnAudio1);
        RspButton2 = findViewById(R.id.RspBtnAudio2);
        RspButton3 = findViewById(R.id.RspBtnAudio3);
        songName = findViewById(R.id.txtSname);
        startTime = findViewById(R.id.txtStartTime);
        songTime = findViewById(R.id.txtSongTime);


        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);

        String curAudio;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            curAudio = String.valueOf((int) bundle.get("file"));

        } else {
            curAudio = databaseHelper.getId(date);
        }

        MediaPlayer();
        answers = databaseHelper.displayAnswers(curAudio);//returns list with possible responses of note'

        if (answers.length == 1) {
            RspButton2.setVisibility(Button.GONE);
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton1.getText().toString(), curAudio);
                }
            });

        } else if (answers.length == 2) {
            RspButton3.setVisibility(Button.GONE);
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curAudio);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curAudio);
                }
            });

        } else if (answers.length == 3) {
            RspButton1.setText(answers[0]);
            RspButton2.setText(answers[1]);
            RspButton3.setText(answers[2]);

            RspButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    databaseHelper.addResponse(RspButton1.getText().toString(), curAudio);
                }
            });
            RspButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton2.getText().toString(), curAudio);
                }
            });
            RspButton3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    databaseHelper.addResponse(RspButton3.getText().toString(), curAudio);
                }
            });
        }
    }

    private void MediaPlayer() {
        Date todaysdate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm");
        String date = format.format(todaysdate);
        String curAudio;
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            curAudio = String.valueOf((int) bundle.get("file"));

        } else {
            curAudio = databaseHelper.getId(date);
        }

        songName.setText(databaseHelper.getNoteTitle(curAudio));
        songPrgs = (SeekBar) findViewById(R.id.sBar);
        songPrgs.setClickable(false);
        pausebtn.setEnabled(false);

        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    FileDescriptor fd = null;
                    String audioPath = databaseHelper.getPath(curAudio).toString();
                    FileInputStream fis = new FileInputStream(audioPath);
                    fd = fis.getFD();

                    if (fd != null) {
                        mPlayer = new MediaPlayer();
                        mPlayer.setDataSource(fd);
                        mPlayer.prepare();
                        mPlayer.start();
                        Toast.makeText(AudioActivity.this, "Playing Audio", Toast.LENGTH_SHORT).show();
                        //mPlayer.start();
                        eTime = mPlayer.getDuration();
                        sTime = mPlayer.getCurrentPosition();
                        if (oTime == 0) {
                            songPrgs.setMax(eTime);
                            oTime = 1;
                        }
                        songTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(eTime),
                                TimeUnit.MILLISECONDS.toSeconds(eTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(eTime))));
                        startTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                                TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
                        songPrgs.setProgress(sTime);
                        hdlr.postDelayed(UpdateSongTime, 100);
                        pausebtn.setEnabled(true);
                        playbtn.setEnabled(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayer.pause();
                pausebtn.setEnabled(false);
                playbtn.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Pausing Audio", Toast.LENGTH_SHORT).show();
            }
        });
        forwardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((sTime + fTime) <= eTime) {
                    sTime = sTime + fTime;
                    mPlayer.seekTo(sTime);
                } else {
                    Toast.makeText(getApplicationContext(), "Cannot jump forward 5 seconds", Toast.LENGTH_SHORT).show();
                }
                if (!playbtn.isEnabled()) {
                    playbtn.setEnabled(true);
                }
            }
        });
        backwardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((sTime - bTime) > 0) {
                    sTime = sTime - bTime;
                    mPlayer.seekTo(sTime);
                } else {
                    Toast.makeText(getApplicationContext(), "Cannot jump backward 5 seconds", Toast.LENGTH_SHORT).show();
                }
                if (!playbtn.isEnabled()) {
                    playbtn.setEnabled(true);
                }
            }
        });

    }

    private Runnable UpdateSongTime = new Runnable() {
        @Override
        public void run() {
            sTime = mPlayer.getCurrentPosition();
            startTime.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(sTime),
                    TimeUnit.MILLISECONDS.toSeconds(sTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(sTime))));
            songPrgs.setProgress(sTime);
            hdlr.postDelayed(this, 100);
        }
    };


}
